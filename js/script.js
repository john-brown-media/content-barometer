// CREATE PAGE CLASS
	
	var $cur
	var $next
	var $prev
	var init
	var $body = $('body')

	var $quiz = {}
	$quiz.total1 = 0
	$quiz.total2 = 0
	$quiz.num = 29
	$quiz.answered = 0
	$quiz.top = 1

	var $stats = {}
	$stats._1 = {}
	$stats._2 = {}
	$stats._1[1] = "87% of UK marketers are using content marketing"
	$stats._1[2] = "74% of UK organisations have small (or one-person) marketing/ content marketing team serving the entire organisation"
	$stats._1[3] = "54% of UK marketers say they are unsure or it is not clear what effective or successful content marketing looks like"
	$stats._1[4] = "85% of UK marketers say high quality content was a key factor in the success of their content marketing"
	$stats._1[5] = "72% of UK marketers say strategy development was a key factor in the success of their content marketing"
	$stats._2[1] = "25% of UK marketers say they plan to document a content strategy in the next 12 months"
	$stats._2[2] = "79% of UK marketers plan to operate content marketing as an ongoing business process, not simply a campaign"
	$stats._2[3] = "79% of UK marketers expect to increase content production in the next 12 months"
	$stats._2[4] = "82% of UK marketers will focus on lead generation as a content marketing goal in the next 12 months"
	$stats._2[5] = "53% of UK marketers plan to increase their content marketing spend over the next 12 months"

	var $types = {}
	$types._1 = {}
	$types._1.s1 = {}
	$types._1.s2 = {}
	$types._2 = {}
	$types._2.s1 = {}
	$types._2.s2 = {}
	$types._3 = {}
	$types._3.s1 = {}
	$types._3.s2 = {}
	$types._4 = {}
	$types._4.s1 = {}
	$types._4.s2 = {}
	$types._5 = {}
	$types._5.s1 = {}
	$types._5.s2 = {}
	$types._1.title = "Reactive"
	$types._2.title = "Tactical"
	$types._3.title = "Integrated"
	$types._4.title = "Strategic"
	$types._5.title = "Intrinsic"
	$types._1.s1.desc = "<p>Oh dear. Your approach to content is ad hoc with little thought to continued management or maintenance of output. Just think of the cumulative impact a consistent approach would achieve!</p><p>Always start with the audience. Create and distribute personas for your team to work from to ensure your content is targeting the right audience. Also think about your brand. Create an editorial statement and share with the wider business to ensure the brand’s purpose is acknowledged across all producers. </p>"
	$types._1.s2.desc = "<p>Hmm. Your content production is not likely to be based on insight or data because you use different producers, each with their own ideas. Your content may follow trends without understanding the relevance to the brand or audience. </p><p>The next step is to build consistency and encourage collaboration between individual content producers. Define common processes for them to deliver to and ensure they all understand your brand and audience.</p>"
	$types._2.s1.desc = "<p>Each channel is likely to be siloed and have its own publishing plans. Although you do bear your customer in mind, perception of who they are may vary across the team. This just needs some extra attention. </p><p>Consider omnichannel briefing, workflow and sign off to help people work in collaboration. Maybe even put your audience personas or content pillars as posters on the walls so everyone has a common view of who and what you are trying to achieve.</p>"
	$types._2.s2.desc = "<p>Each channel has their own processes but anything new is likely to be abandoned under the pressure of deadlines, changing requirements or resistance to change. It’s worthwhile to dedicate a little more time here to avoid duplication and bottlenecks – think of the cost savings!</p><p>There can be considerable cost savings when producing for multiple channels, in addition to a consistent look and feel. Management needs a firm commitment to implement and follow standard templates, style guide/tone of voice documents and project management practices. </p>"
	$types._3.s1.desc = "<p>You have a good foundation to build on, an integrated approach to publishing and routine quality assurance activities that are incorporated into every project. Planning is in place although not always down to the level of channel plans with the same content being distributed on various platforms. There may be inconsistent measurement in the impact of content. </p><p>When channel planning, understand that different audiences are dominant on different platforms, even down to what social media sites they prefer and how they behave on them. Start thinking about how you can use content mapping across different channels and calls to action to determine what you want your customers’ journeys to be. By evangelising customer studies, you can incorporate customer needs into content development and calls to action. </p>"
	$types._3.s2.desc = "<p>There’s a solid foundation to build on. You have a number of channels you use and produce content for but may be distributing the same content across all channels. Think about how you would tell the same story across different channels; would you use video, copy or images? All can be produced at the same time, with common assets to ensure brand consistency, but will play to the attributes of the publishing platform. </p><p>Through planning, you will understand that different audiences have preferred platforms. Ensure content briefing happens early enough in the calendar to enable collaboration between teams and complimentary channel plans to be developed in time for production deadlines. Finally, rather than just focus on what’s next, assess the reversioning of existing content.</p>"
	$types._4.s1.desc = "<p>Well done. You have a strategic approach to content. Leadership may change without a loss of commitment to planning, and quality assurance and increasingly sophisticated methods of measurement, analysis, ROI are being used and fed back into planning and the production process.</p><p>Let’s perfect what you already have. How aligned is your content strategy with the business strategy? Achieving content goals is one thing, but are you feeding back into product and service design? How can content better serve the wider business?</p>"
	$types._4.s2.desc = "<p>Well done. Content is recognised as effective by the wider organisation and is treated as a corporate asset with long term ambitions and managed with appropriate checks in processes and output. It is customer focused and agile, able to adapt to changing market forces while remaining on brand and objective focused. </p><p>So how can you improve the content? Consider personalisation using an automated approached to adapting it, reconfiguring or repurposing assets for any platform or behaviour of individual customers.</p>"
	$types._5.s1.desc = "<p>Congratulations! Content is a key aspect of your organisation’s business strategy and is recognised for its complexity and impact on brand definition and positive customer experience. Sustaining it is key and you understand the need for business continuity planning for ongoing success.</p><p>Fancy a glass of wine to celebrate and talk content?</p>"
	$types._5.s2.desc = "<p>Congratulations! You appreciate a customer’s continuing lifecycle and use personalisation to ensure your customers get what they want, when they want it, to ensure maximum return. </p><p>Fancy a glass of wine to celebrate and talk content?</p>"
	$types._1.cta = "<h2>Would you like us to design and deliver a session to set a content vision for the future? Create an editorial statement for your brand?</h2><p>We also produce governance documents like Personas from audience research, Tone of Voice and Brand Guidelines for content producers.</p><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>"
	$types._2.cta = "<h2>Would you like us to design and deliver a workshop to help set a content vision for the future? Map your workflows between teams for increased productivity?</h2><p>We also produce governance documents like Personas from audience research, Tone of Voice and Brand Guidelines for content producers.</p><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>"
	$types._3.cta = "<h2>Would you like us to design and deliver a session to map a customer’s content journey between channels? Or assess your production workflows between teams and design an omni-channel briefing process?</h2><p>We write content strategies specific to brand and audience that will enhance your business.</p><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>"
	$types._4.cta = "<h2>We support strategy throughout the content lifecycle and customer journey.</h2><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>"
	$types._5.cta = "<h2>Wine it is then. </h2><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>"

	var $ans = {}
	$ans.p1 = 0
	$ans.p2 = 0
	$ans.p3 = 0
	$ans.p4 = 0
	$ans.p5 = 0
	$ans.p6 = 0
	$ans.p7 = 0
	$ans.p8 = 0
	$ans.p9 = 0
	$ans.p10 = 0
	$ans.p11 = 0
	$ans.p12 = 0
	$ans.p13 = 0
	$ans.p14 = 0
	$ans.p15 = 0
	$ans.p16 = 0
	$ans.p17 = 0
	$ans.p18 = 0
	$ans.p19 = 0
	$ans.p20 = 0
	$ans.p21 = 0
	$ans.p22 = 0
	$ans.p23 = 0
	$ans.p24 = 0
	$ans.p25 = 0
	$ans.p26 = 0
	$ans.p27 = 0
	$ans.p28 = 0
	$ans.p29 = 0
	$ans.p30 = 0

	var $page = {}
	$page.p1 = {}
	$page.p2 = {}
	$page.p3 = {}
	$page.p4 = {}
	$page.p5 = {}
	$page.p6 = {}
	$page.p7 = {}
	$page.p8 = {}
	$page.p9 = {}
	$page.p10 = {}
	$page.p11 = {}
	$page.p12 = {}
	$page.p13 = {}
	$page.p14 = {}
	$page.p15 = {}
	$page.p16 = {}
	$page.p17 = {}
	$page.p18 = {}
	$page.p19 = {}
	$page.p20 = {}
	$page.p21 = {}
	$page.p22 = {}
	$page.p23 = {}
	$page.p24 = {}
	$page.p25 = {}
	$page.p26 = {}
	$page.p27 = {}
	$page.p28 = {}
	$page.p29 = {}
	$page.p30 = {}
	$page.p31 = {}


	// DEFINE PAGES

	$page.p1.name = 1
	$page.p1.type = 0 //0 home 1 q 2 end
	$page.p1.section = 1
	$page.p1.status = 1
	$page.p1.visited = 0
	$page.p1.question = 'This is the question 1'
	$page.p1.answer = 1

	$page.p2.name = 2
	$page.p2.type = 1 //0 home 1 q 2 end
	$page.p2.title = 'Strategy & Planning'
	$page.p2.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p2.section = 1
	$page.p2.status = 0 
	$page.p2.visited = 0
	$page.p2.question = 'There is a well-publicised and documented content strategy outlining business case and business plan, as well as governance documents for producing content'
	$page.p2.answer = 0

	$page.p3.name = 3
	$page.p3.type = 1 //0 home 1 q 2 end
	$page.p3.title = 'Strategy & Planning'
	$page.p3.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p3.section = 1
	$page.p3.status = 0 
	$page.p3.visited = 0
	$page.p3.question = 'Content has a role to play in delivering the organisation’s vision'
	$page.p3.answer = 0

	$page.p4.name = 4
	$page.p4.type = 1 //0 home 1 q 2 end
	$page.p4.title = 'Strategy & Planning'
	$page.p4.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p4.section = 1
	$page.p4.status = 0 
	$page.p4.visited = 0
	$page.p4.question = 'Content strategy is closely aligned to the business strategy'
	$page.p4.answer = 0

	$page.p5.name = 5
	$page.p5.type = 1 //0 home 1 q 2 end
	$page.p5.title = 'Strategy & Planning'
	$page.p5.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p5.section = 1
	$page.p5.status = 0 
	$page.p5.visited = 0
	$page.p5.question = 'The role of content is recognised as effective by the wider organisation'
	$page.p5.answer = 0

	$page.p6.name = 6
	$page.p6.type = 1 //0 home 1 q 2 end
	$page.p6.title = 'Strategy & Planning'
	$page.p6.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p6.section = 1
	$page.p6.status = 0 
	$page.p6.visited = 0
	$page.p6.question = 'Audiences are regularly defined using qualitative and quantitative research'
	$page.p6.answer = 0

	$page.p7.name = 7
	$page.p7.type = 1 //0 home 1 q 2 end
	$page.p7.title = 'Strategy & Planning'
	$page.p7.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p7.section = 1
	$page.p7.status = 0 
	$page.p7.visited = 0
	$page.p7.question = 'Your organisation understands the the value gained by customer retention'
	$page.p7.answer = 0

	$page.p8.name = 8
	$page.p8.type = 1 //0 home 1 q 2 end
	$page.p8.title = 'Strategy & Planning'
	$page.p8.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p8.section = 1
	$page.p8.status = 0 
	$page.p8.visited = 0
	$page.p8.question = 'There is an understanding in how content is used on different platforms and for different audiences'
	$page.p8.answer = 0

	$page.p9.name = 9
	$page.p9.type = 1 //0 home 1 q 2 end
	$page.p9.title = 'Strategy & Planning'
	$page.p9.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p9.section = 1
	$page.p9.status = 0 
	$page.p9.visited = 0
	$page.p9.question = 'Content is mapped to the purchase path/customer journey'
	$page.p9.answer = 0

	$page.p10.name = 10
	$page.p10.type = 1 //0 home 1 q 2 end
	$page.p10.title = 'Strategy & Planning'
	$page.p10.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p10.section = 1
	$page.p10.status = 0 
	$page.p10.visited = 0
	$page.p10.question = 'There is an editorial plan'
	$page.p10.answer = 0

	$page.p11.name = 11
	$page.p11.type = 1 //0 home 1 q 2 end
	$page.p11.title = 'Strategy & Planning'
	$page.p11.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p11.section = 1
	$page.p11.status = 0 
	$page.p11.visited = 0
	$page.p11.question = 'Editorial plans are adapted based on insight and analysis or current events'
	$page.p11.answer = 0

	$page.p12.name = 12
	$page.p12.type = 1 //0 home 1 q 2 end
	$page.p12.title = 'Strategy & Planning'
	$page.p12.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p12.section = 1
	$page.p12.status = 0 
	$page.p12.visited = 0
	$page.p12.question = 'Existing content is reviewed, repurposed or archived on a regular basis'
	$page.p12.answer = 0

	$page.p13.name = 13
	$page.p13.type = 1 //0 home 1 q 2 end
	$page.p13.title = 'Strategy & Planning'
	$page.p13.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p13.section = 1
	$page.p13.status = 0 
	$page.p13.visited = 0
	$page.p13.question = 'There is a long term roadmap for the continued development of content to better serve the business'
	$page.p13.answer = 0

	$page.p14.name = 14
	$page.p14.type = 1 //0 home 1 q 2 end
	$page.p14.title = 'Strategy & Planning'
	$page.p14.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p14.section = 1
	$page.p14.status = 0 
	$page.p14.visited = 0
	$page.p14.question = 'Content automation is used to create a personalised experience for the customer'
	$page.p14.answer = 0

	$page.p15.name = 15
	$page.p15.type = 1 //0 home 1 q 2 end
	$page.p15.title = 'Strategy & Planning'
	$page.p15.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p15.section = 1
	$page.p15.status = 0 
	$page.p15.visited = 0
	$page.p15.question = 'There is understanding of content\'s ROI which is indicated in the budget available for production'
	$page.p15.answer = 0

	$page.p16.name = 16
	$page.p16.type = 1 //0 home 1 q 2 end
	$page.p16.title = 'Production'
	$page.p16.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p16.section = 2
	$page.p16.status = 0 
	$page.p16.visited = 0
	$page.p16.question = 'There is a widespread understanding in the vision and purpose of content'
	$page.p16.answer = 0

	$page.p17.name = 17
	$page.p17.type = 1 //0 home 1 q 2 end
	$page.p17.title = 'Production'
	$page.p17.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p17.section = 2
	$page.p17.status = 0 
	$page.p17.visited = 0
	$page.p17.question = 'The content team is structured and has clearly defined roles within it'
	$page.p17.answer = 0

	$page.p18.name = 18
	$page.p18.type = 1 //0 home 1 q 2 end
	$page.p18.title = 'Production'
	$page.p18.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p18.section = 2
	$page.p18.status = 0 
	$page.p18.visited = 0
	$page.p18.question = 'There are clear communication lines and collaboration between teams e.g. online/offline, marketing/PR etc.'
	$page.p18.answer = 0

	$page.p19.name = 19
	$page.p19.type = 1 //0 home 1 q 2 end
	$page.p19.title = 'Production'
	$page.p19.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p19.section = 2
	$page.p19.status = 0 
	$page.p19.visited = 0
	$page.p19.question = 'There is a consistent briefing process'
	$page.p19.answer = 0

	$page.p20.name = 20
	$page.p20.type = 1 //0 home 1 q 2 end
	$page.p20.title = 'Production'
	$page.p20.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p20.section = 2
	$page.p20.status = 0 
	$page.p20.visited = 0
	$page.p20.question = 'Thought is given to the purpose of each piece of content and its call to action'
	$page.p20.answer = 0

	$page.p21.name = 21
	$page.p21.type = 1 //0 home 1 q 2 end
	$page.p21.title = 'Production'
	$page.p21.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p21.section = 2
	$page.p21.status = 0 
	$page.p21.visited = 0
	$page.p21.question = 'Customer insight and data is used by the production team to inform the type of content that is produced'
	$page.p21.answer = 0

	$page.p22.name = 22
	$page.p22.type = 1 //0 home 1 q 2 end
	$page.p22.title = 'Production'
	$page.p22.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p22.section = 2
	$page.p22.status = 0 
	$page.p22.visited = 0
	$page.p22.question = 'The content teams regularly interact with customers e.g. through social channels'
	$page.p22.answer = 0

	$page.p23.name = 23
	$page.p23.type = 1 //0 home 1 q 2 end
	$page.p23.title = 'Production'
	$page.p23.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p23.section = 2
	$page.p23.status = 0 
	$page.p23.visited = 0
	$page.p23.question = 'The content team is able to quote deadlines and deliver work on time'
	$page.p23.answer = 0

	$page.p24.name = 24
	$page.p24.type = 1 //0 home 1 q 2 end
	$page.p24.title = 'Production'
	$page.p24.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p24.section = 2
	$page.p24.status = 0 
	$page.p24.visited = 0
	$page.p24.question = 'The content team has templates for regularly produced content e.g. email newsletters, blog pages, etc.'
	$page.p24.answer = 0

	$page.p25.name = 25
	$page.p25.type = 1 //0 home 1 q 2 end
	$page.p25.title = 'Production'
	$page.p25.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p25.section = 2
	$page.p25.status = 0 
	$page.p25.visited = 0
	$page.p25.question = 'Content is produced for the platform it is distributed on, utilising the attributes of the platform e.g. individual social channels, mobile/tablet, etc.'
	$page.p25.answer = 0

	$page.p26.name = 26
	$page.p26.type = 1 //0 home 1 q 2 end
	$page.p26.title = 'Production'
	$page.p26.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p26.section = 2
	$page.p26.status = 0 
	$page.p26.visited = 0
	$page.p26.question = 'Time for iteration of content output is accounted for in the delivery timeline'
	$page.p26.answer = 0

	$page.p27.name = 27
	$page.p27.type = 1 //0 home 1 q 2 end
	$page.p27.title = 'Production'
	$page.p27.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p27.section = 2
	$page.p27.status = 0 
	$page.p27.visited = 0
	$page.p27.question = 'The team who produce content (internal and external) understand the company’s tone of voice and brand guidelines'
	$page.p27.answer = 0

	$page.p28.name = 28
	$page.p28.type = 1 //0 home 1 q 2 end
	$page.p28.title = 'Production'
	$page.p28.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p28.section = 2
	$page.p28.status = 0 
	$page.p28.visited = 0
	$page.p28.question = 'The commissioner of content understands content development and production'
	$page.p28.answer = 0

	$page.p29.name = 29
	$page.p29.type = 1 //0 home 1 q 2 end
	$page.p29.title = 'Production'
	$page.p29.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p29.section = 2
	$page.p29.status = 0 
	$page.p29.visited = 0
	$page.p29.question = 'There are clear sign off procedures at specific stages throughout the production process'
	$page.p29.answer = 0

	$page.p30.name = 30
	$page.p30.type = 1 //0 home 1 q 2 end
	$page.p30.title = 'Production'
	$page.p30.sub = 'RATE THE FOLLOWING STATEMENTS ON A SCALE OF 1-5.'
	$page.p30.section = 2
	$page.p30.status = 0 
	$page.p30.visited = 0
	$page.p30.question = 'Content performance is measured and fed back into the production process'
	$page.p30.answer = 0

	$page.p31.name = 31
	$page.p31.type = 2 //0 home 1 q 2 end
	$page.p31.title = 'RESULTS PAGE'
	$page.p31.sub = 'This is the RESULTS PAGE'
	$page.p31.section = 1
	$page.p31.status = 0 
	$page.p31.visited = 0
	$page.p31.question = 'THIS IS THE RESULTS PAGE'
	$page.p31.answer = 1

	// FUNCTIONS
	$page.update = function($cur){
		$load = 0
		var $visited = 1
		var $status = $page[$cur].status

		$type = $page[$cur].type

		if ($type === 1) {
			$answer = $('.selector').data('ans')
			if ($answer > 0) {
				if ($status === 0)
					{
						$status = 1
						$quiz.answered++
					}
			}
			$page[$cur].answer = $answer
		}
				
		$page[$cur].status = $status

		if ($status === 1) {
			// if page has been answered
			if ($page[$cur].name > $quiz.top) {
				$quiz.top = $page[$cur].name
				i = $quiz.top 
				$('.progress').removeClass('_0 _1 _2 _3 _4 _5 _6 _7 _8 _9 _10 _11 _12 _13 _14 _15 _16 _171 _18 _19 _20 _21 _23 _23 _24 _25 _26 _27 _28 _29')
				$('.progress').addClass('_'+i)
			}
		}
		$page[$cur].status = $status
		$page[$cur].visited = $visited
		$ans['p'+parseInt($page[$cur].name - 1)] = $page[$cur].answer

		// return $load
	}

	$page.load = function($pg){

		// update current page
		if (init === 1) {
			$page.update($cur)
		}else{
			init = 1
		}

		// ##### TYPE CONDITIONAL ######
		
		$type = $page[$pg].type
		
		if ($type === 0) {

			// homepage code

			$num = $page[$pg].name
			$cur = 'p'+$page[$pg].name
			$next = 'p'+($page[$pg].name+1)
			$prev = 'p'+($page[$pg].name-1)
			$status = $page[$pg].status
			$visited = $page[$pg].visited
			
			$('.next').data('page',$next)
			
			$body.data('status', $status)
			$body.removeClass('type_0 type_1 type_2')
			$body.addClass('type_'+$type)

		}else if ($type === 1) {

			// quiz page code

			$num = $page[$pg].name
			$cur = 'p'+$page[$pg].name
			$next = 'p'+($page[$pg].name+1)
			$prev = 'p'+($page[$pg].name-1)
			$status = $page[$pg].status
			$visited = $page[$pg].visited
			$answer = $page[$pg].answer
			$title = $page[$pg].title
			$sub = $page[$pg].sub
			$question = $page[$pg].question
			
			$body.data('status', $status)
			$body.removeClass('type_0 type_1 type_2')
			$body.addClass('type_'+$type)

			$('.quiz .top .header').text($title)
			$('.quiz .top .subtitle').text($sub)

			$('.next').data('page',$next)
			$('.prev').data('page',$prev)

			$('.quiz .question .inner').text($question)

			$('.quiz .selector').removeClass('_0 _1 _2 _3 _4 _5')
				.data('ans', $answer)
				.addClass('_'+$answer)


		}else if ($type === 2) {
			// results page code
			
			// check if all questions have been answered
			if ($quiz.num === $quiz.answered) {
				
				// load the quiz page				
				$num = $page[$pg].addClass
				$cur = 'p'+$page[$pg].name
				$body.data('status', $status)
				$body.data('visited', $visited)
				$body.data('page', $num)
				
				$body.removeClass('type_0 type_1 type_2')
				$body.addClass('type_'+$type)

				$page.result_load()

			}else{
				// show error message that quiz is not complete
			}

		}

		// HIDE NEXT BUTTON ON CURRENT PAGE
		if ($page[$pg].name > $quiz.top ) {
			$('.next').addClass('hide')
		} else {
			$('.next').removeClass('hide')
		}	
		
	}

	$page.result_load = function(){
		var $section1_result = 0 
		var $section2_result = 0

		for (var i = 14 - 1; i >= 0; i--) {
			q1 = i + 1
			$section1_result = $section1_result + parseInt($ans['p'+q1])
		}

		for (var x = 29 - 1; x >= 14; x--) {
			q2 = x + 1
			$section2_result = $section2_result + parseInt($ans['p'+q2])
		}		

		$section1_per = $section1_result / 70 * 100
		$section2_per = $section2_result / 75 * 100

		if ($section1_per <= 20) {
			$section1_type = 1
			$section1_ang = 0
		}else if ($section1_per <= 40) {
			$section1_type = 2
			$section1_ang = 45
		}else if ($section1_per <= 60) {
			$section1_type = 3
			$section1_ang = 90
		}else if ($section1_per <= 80) {
			$section1_type = 4
			$section1_ang = 135
		}else if ($section1_per <= 100) {
			$section1_type = 5
			$section1_ang = 180
		}

		if ($section2_per <= 20) {
			$section2_type = 1
			$section2_ang = 0
		}else if ($section2_per <= 40) {
			$section2_type = 2
			$section2_ang = 45
		}else if ($section2_per <= 60) {
			$section2_type = 3
			$section2_ang = 90
		}else if ($section2_per <= 80) {
			$section2_type = 4
			$section2_ang = 135
		}else if ($section2_per <= 100) {
			$section2_type = 5
			$section2_ang = 180
		}

		$rand1 = Math.floor(Math.random() * 5) + 1
		$rand2 = Math.floor(Math.random() * 5) + 1
		
		$('.results ._1 .dial .arrow').animateRotate($section1_ang)
		$('.results ._2 .dial .arrow').animateRotate($section2_ang)

		$('.results .section._1 .dial').addClass('_'+$section1_type)
		$('.results .section._2 .dial').addClass('_'+$section2_type)

		$('.results .section._1 .result_title').text($types['_'+$section1_type].title)
		$('.results .section._1 .stat .txt').text($stats._1[$rand1])
		$('.results .section._1 .result_txt').html($types['_'+$section1_type].s1.desc)

		$('.results .section._2 .result_title').text($types['_'+$section2_type].title)
		$('.results .section._2 .stat .txt').text($stats._2[$rand2])
		$('.results .section._2 .result_txt').html($types['_'+$section2_type].s2.desc)
		
		$('.results .section._1 .findoutmore').attr('href', 'how-can-we-help.php?t='+$section1_type+'&s=1');
		$('.results .section._2 .findoutmore').attr('href', 'how-can-we-help.php?t='+$section2_type+'&s=2');
	}

	// NAVIGATION LINKS
	$('.next,.prev').click(function(){
		$new_pg = $(this).data('page')
		if ($page[$cur].type === 0 || $page[$new_pg].visited === 1)
		{
			$page.load($new_pg)
		}
	})

	// PROGRESS BAR CLICK
	$('.progress .block').click(function(){
		$new_pg = $(this).data('page')
		$new_pg++
		$new_pg = 'p'+$new_pg

		if ($page[$cur].type === 0 || $page[$new_pg].visited === 1)
		{
			$page.load($new_pg)
		}
	})

	// ANSWER SELECTOR BOX FUNCTIONALITY
	$('.box').click(function(){
		$num = $(this).data('num')
		$('.selector').data('ans', $num).addClass('halt')	
		$page.load($next)
	}).hover(function() {
		$temp_num = $(this).data('num')
		$('.selector').addClass('temp_'+$temp_num)
	}, function() {
		$('.selector').removeClass('temp_1 temp_2 temp_3 temp_4 temp_5')
	});
	$(document).mousemove(function(event) {
		if ($('.halt').length > 0) {
			$('.selector').removeClass('halt')	
		}
	});



	// ANIMATE FUNCTION FOR DIAL
	$.fn.animateRotate = function(angle, duration, easing, complete) {
	  return this.each(function() {
	    var $elem = $(this);

	    $({deg: 0}).animate({deg: angle}, {
	      duration: duration,
	      easing: easing,
	      step: function(now) {
	        $elem.css({
	           transform: 'rotate(' + now + 'deg)'
	         });
	      },
	      complete: complete || $.noop
	    });
	  });
	};

	


$(document).ready(function() {
	$page.load('p1')
});