<?php 

if (isset($_GET['t']) && isset($_GET['s'])) {
	$type = $_GET['t'];
	$s = $_GET['s'];
}

if (isset($_POST['email'])) {
	// form successful
	$form = 1;
}

if ($s == 1) {
	$s_name = "Strategy & Planning";
}elseif ($s == 2) {
	$s_name = "Production";
}else {
	$s_name = "Strategy & Planning";
}

if ($type == 1) {
	$t_name = "Reactive";

	$cta = "<h2>Would you like us to design and deliver a session to set a content vision for the future? Create an editorial statement for your brand?</h2><p>We also produce governance documents like Personas from audience research, Tone of Voice and Brand Guidelines for content producers.</p><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>";
}elseif ($type == 2) {
$t_name = "Tactical";

	$cta = "<h2>Would you like us to design and deliver a workshop to help set a content vision for the future? Map your workflows between teams for increased productivity?</h2><p>We also produce governance documents like Personas from audience research, Tone of Voice and Brand Guidelines for content producers.</p><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>";

}elseif ($type == 3) {
	$t_name = "Integrated";

	$cta = "<h2>Would you like us to design and deliver a session to map a customer’s content journey between channels? Or assess your production workflows between teams and design an omni-channel briefing process?</h2><p>We write content strategies specific to brand and audience that will enhance your business.</p><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>";

}elseif ($type == 4) {
	$t_name = "Strategic";

	$cta = "<h2>We support strategy throughout the content lifecycle and customer journey.</h2><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>";

}elseif ($type == 5) {
	$t_name = "Intrinsic";

	$cta = "<h2>Wine it is then. </h2><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>";

}else {
	$t_name = "Reactive";

	$cta = "<h2>Would you like us to design and deliver a session to set a content vision for the future? Create an editorial statement for your brand?</h2><p>We also produce governance documents like Personas from audience research, Tone of Voice and Brand Guidelines for content producers.</p><p>Get in touch below if you would like us to support your team or partner with you on any future content production.</p>";
}


 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>How Can We Help - John Brown Quiz</title>
	<link rel="stylesheet" href="css/screen.css">

	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body data-status="0" data-page="0" class="type_2">


	<section class="results">
		<div class="white_wrap">
			<div class="top">
				<div class="header_bar">
					<div class="logo"><img src="img/logo.png" alt="John Brown"></div>
				</div>
					
				<div class="cta-text">
					
					<h3>your <span class="bold"><?php echo $s_name ?></span> is currently</h3>
					<h2 class="result_title"><?php echo $t_name ?></h2>
					<div class="text">
						<?php echo $cta ?>
					</div>
				</div>
				<div class="form">
					<p class="thanks <?php echo ($form == 1 ? 'show' : 'hide') ?>">Thank you for getting in touch. You'll hear from us soon.</p>
					<form action="" method="post" class="">
						<p>
							<label for="name">Name: </label><input type="text" name="name" id="name" required="required">
						</p>
						<p>
							<label for="email">Email address: </label><input type="email" name="email" id="email" required="required">
						</p>
						<p>
							<label for="tel">Phone number: </label><input type="tel" name="tel" id="tel">
						</p>
						<p>
							<input type="submit" value="Submit" id="submit">
						</p>
					</form>
				</div>

			</div>
		</div>
		<div class="bottom">
			<div class="cta_wrap">
				<p>We'd love to hear from you - <a href="mailto:kate.pickering@johnbrownmedia.com">kate.pickering@johnbrownmedia.com</a></p>
			</div>
		</div>
	</section>

</html>